package shepelev.denis.sovcombank.task.cleanuri.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс с параметрами для ответа
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ShortLinkResponse {
    @JsonProperty("result_url")
    private String resultUrl;
    private String error;
}
