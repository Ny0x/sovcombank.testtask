package shepelev.denis.sovcombank.task.cleanuri.tests;

import shepelev.denis.sovcombank.task.cleanuri.Config;
import shepelev.denis.sovcombank.task.cleanuri.api.LongLinkRequest;
import shepelev.denis.sovcombank.task.cleanuri.api.ShortLinkResponse;
import shepelev.denis.sovcombank.task.cleanuri.utils.TestData;
import shepelev.denis.sovcombank.task.cleanuri.utils.TestDataFactory;

import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Абстрактный класс для всех тестов
 **/
public abstract class AbstractTest {
    LongLinkRequest request; // Один реквест на все тесты внутри тестового класса (чтобы каждый раз новый не создавать)

    public AbstractTest() {
        request = TestDataFactory.createLongLinkRequest();
    }

    /**
     * Реквест + ассерт, который будет использоваться в каждом тесте
     * (в рамках текущей задачи показалось наиболее разумным решением, хоть и не совсем правильным)
     **/
    public void makePostRequestAndAssertResponse(List<TestData> inputs) {
        setSpecifications();
        for (TestData input : inputs) {
            System.out.printf("%s: %s%n", input.getDescription(), input.getUrl());
            request.setUrl(input.getUrl());

            ShortLinkResponse response = given()
                    .body(request)
                    .when()
                    .post(Config.apiVersion)
                    .then()
                    .log().body()
                    .extract().as(ShortLinkResponse.class);

            assertEquals(input.getExpectedResultUrl(), response.getResultUrl());
            assertEquals(input.getExpectedError(), response.getError());

            waitBeforeNextRequest();
        }
    }

    /**
     * Ожидание перед следующим реквестом (2 реквеста в секунду, из документации)
     **/
    private void waitBeforeNextRequest() {
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Выставить соответствующую спецификацию
     **/
    public abstract void setSpecifications();
}
