package shepelev.denis.sovcombank.task.cleanuri;

/**
 * Класс для конфигурационных данных
 **/
public class Config {
    public static final String baseUrl = "https://cleanuri.com";
    public static final String apiVersion = "/api/v1/shorten";
    public static final String inputsFile = "src/test/java/shepelev/denis/sovcombank/task/cleanuri/resources/inputs.csv";
}
