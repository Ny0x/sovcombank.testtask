package shepelev.denis.sovcombank.task.cleanuri.api;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс с параметрами для запроса
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class LongLinkRequest {
    private String url;
}
