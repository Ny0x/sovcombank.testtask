package shepelev.denis.sovcombank.task.cleanuri.tests;

import org.junit.Test;
import shepelev.denis.sovcombank.task.cleanuri.api.Specifications;
import shepelev.denis.sovcombank.task.cleanuri.utils.TestDataFactory;

/**
 * Класс с позитивными тестами
 **/
public class PositiveTests extends AbstractTest {
    @Test
    public void getShortedUriWhenUrlIsValid() {
        makePostRequestAndAssertResponse(TestDataFactory.getTestDataForPositiveTests());
    }

    @Override
    public void setSpecifications() {
        Specifications.installSpecification(Specifications.requestSpecification(),
                Specifications.responseSpecificationOk200());
    }
}
