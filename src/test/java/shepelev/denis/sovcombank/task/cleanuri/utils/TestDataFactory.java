package shepelev.denis.sovcombank.task.cleanuri.utils;

import com.opencsv.bean.CsvToBeanBuilder;
import shepelev.denis.sovcombank.task.cleanuri.Config;
import shepelev.denis.sovcombank.task.cleanuri.api.LongLinkRequest;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Фабрика для работы с тестовыми данными
 **/
public class TestDataFactory {
    private static List<TestData> inputs;

    // Инциализация статическая, чтобы при запуске тестов один раз собрать инпуты и забыть
    static {
        try {
            inputs = new CsvToBeanBuilder(new FileReader(Config.inputsFile))
                    .withType(TestData.class)
                    .build()
                    .parse();
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
        inputs = inputs.stream().peek(x -> {
            if (x.getExpectedError().equals("")) x.setExpectedError(null);
            if (x.getExpectedResultUrl().equals("")) x.setExpectedResultUrl(null);
        }).collect(Collectors.toList());
        inputs.remove(0);
    }

    /**
     * Создать новый реквест
     **/
    public static LongLinkRequest createLongLinkRequest() {
        return new LongLinkRequest();
    }

    /**
     * Выборка инпутов для позитивных тестов
     **/
    public static List<TestData> getTestDataForPositiveTests() {
        return inputs.stream().filter(x -> x.getEnabled().equals("TRUE")
                && x.getExpectedError() == null).collect(Collectors.toList());
    }

    /**
     * Выборка инпутов для негативных тестов
     **/
    public static List<TestData> getTestDataForNegativeTests() {
        return inputs.stream().filter(x -> x.getEnabled().equals("TRUE")
                && x.getExpectedError() != null).collect(Collectors.toList());
    }
}
