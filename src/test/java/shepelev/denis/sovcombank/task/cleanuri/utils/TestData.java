package shepelev.denis.sovcombank.task.cleanuri.utils;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Класс для парсинга тестовых данных с внешнего файла
 **/
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TestData {
    @CsvBindByPosition(position = 0)
    private String enabled;
    @CsvBindByPosition(position = 1)
    private String description;
    @CsvBindByPosition(position = 2)
    private String url;
    @CsvBindByPosition(position = 3)
    private String expectedResultUrl;
    @CsvBindByPosition(position = 4)
    private String expectedError;
}
