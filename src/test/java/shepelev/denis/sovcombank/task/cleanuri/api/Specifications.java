package shepelev.denis.sovcombank.task.cleanuri.api;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import shepelev.denis.sovcombank.task.cleanuri.Config;

/**
 * Класс для работы со спецификациями
 **/
public class Specifications {
    /**
     * Выставить URL и тип содержимого для всех реквестов
     **/
    public static RequestSpecification requestSpecification() {
        return new RequestSpecBuilder()
                .setBaseUri(Config.baseUrl)
                .setContentType(ContentType.JSON)
                .build();
    }

    /**
     * Код 200 на валидные реквесты
     **/
    public static ResponseSpecification responseSpecificationOk200() {
        return new ResponseSpecBuilder()
                .expectStatusCode(200)
                .build();
    }

    /**
     * Код 400 на невалидные реквесты
     **/
    public static ResponseSpecification responseSpecificationBadRequest400() {
        return new ResponseSpecBuilder()
                .expectStatusCode(400)
                .build();
    }

    /**
     * Выставить спеификации
     **/
    public static void installSpecification(RequestSpecification requestSpecification,
                                            ResponseSpecification responseSpecification) {
        RestAssured.requestSpecification = requestSpecification;
        RestAssured.responseSpecification = responseSpecification;
    }
}
