package shepelev.denis.sovcombank.task.cleanuri.tests;

import org.junit.Test;
import shepelev.denis.sovcombank.task.cleanuri.api.Specifications;
import shepelev.denis.sovcombank.task.cleanuri.utils.TestDataFactory;

/**
 * Класс с негативными тестами
 **/
public class NegativeTests extends AbstractTest {
    @Test
    public void getShortedUriWhenUrlIsInvalid() {
        makePostRequestAndAssertResponse(TestDataFactory.getTestDataForNegativeTests());
    }

    @Override
    public void setSpecifications() {
        Specifications.installSpecification(Specifications.requestSpecification(),
                Specifications.responseSpecificationBadRequest400());
    }
}
