package shepelev.denis.sovcombank.task.site.pages;

import org.openqa.selenium.WebDriver;

/**
 * Абстрактная страница
 **/
abstract public class AbstractPage {
    protected static WebDriver driver;

    public static void setDriver(WebDriver webDriver) {
        driver = webDriver;
    }
}
