package shepelev.denis.sovcombank.task.site.tests;

import org.junit.Test;
import shepelev.denis.sovcombank.task.site.pages.OfficePage;
import shepelev.denis.sovcombank.task.site.utils.TestData;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class OfficePageTests extends AbstractTest {
    /**
     * Проверка фильтрации (во всех адресах один и тот же паттерн) + количество результатов
     **/
    @Test
    public void checkOfficesAndAtmsAddresses() {
        OfficePage page = new OfficePage();

        List<String> officesAddresses = page.getListOfOfficesAddresses();
        officesAddresses.forEach(x -> assertTrue(x.contains(TestData.ADDRESS_PATTERN)));
        System.out.println("Всего офисов: " + officesAddresses.size());


        List<String> atmsAddresses = page.getListOfAtmsAddresses();
        atmsAddresses.forEach(x -> assertTrue(x.contains(TestData.ADDRESS_PATTERN)));
        System.out.println("Всего банкоматов: " + atmsAddresses.size());
    }
}
