package shepelev.denis.sovcombank.task.site.pages;

import lombok.Getter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import shepelev.denis.sovcombank.task.site.Config;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Страница с офисами и банкоматами. Прописан лишь кусочек, необходимый для задания
 **/
@Getter
public class OfficePage extends AbstractPage {
    @FindBy(xpath = "//div[contains(@class, 'office-info-container')]")
    WebElement pointsContainer;

    @FindBy(xpath = "//button[text()='Банкоматы']")
    WebElement atmsButton;

    public OfficePage() {
        driver.get(Config.officesPageUrl);
        PageFactory.initElements(driver, this);
    }

    public List<String> getListOfOfficesAddresses() {
        return getListOfPointsAddressesFromContainer();
    }

    public List<String> getListOfAtmsAddresses() {
        atmsButton.click();
        return getListOfPointsAddressesFromContainer();
    }

    /**
     * Достать из контейнера все адреса (для офисов или банкоматов)
     **/
    private List<String> getListOfPointsAddressesFromContainer() {
        return pointsContainer
                .findElements(By.xpath("//div[contains(@class, 'text')][2]"))
                .stream().map(WebElement::getText).collect(Collectors.toList());
    }
}
