package shepelev.denis.sovcombank.task.randomuser.utils;

import shepelev.denis.sovcombank.task.randomuser.api.Name;

/**
 * Фабрика для тестовых данных (задумывалась как конструктор сущностей для использования в тестах)
 **/
public class TestDataFactory {
    public static Name getTestDataName() {
        return new Name("Miss", "آیناز", "کریمی");
    }
}
