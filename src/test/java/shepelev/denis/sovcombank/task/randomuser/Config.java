package shepelev.denis.sovcombank.task.randomuser;

/**
 * Конфигурационный класс
 **/
public class Config {
    public static final String baseUrl = "https://randomuser.me";
    public static final String apiVersion = "/api/";
}