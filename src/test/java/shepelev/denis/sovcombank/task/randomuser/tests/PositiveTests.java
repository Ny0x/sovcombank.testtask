package shepelev.denis.sovcombank.task.randomuser.tests;

import org.junit.BeforeClass;
import org.junit.Test;
import shepelev.denis.sovcombank.task.randomuser.api.Root;
import shepelev.denis.sovcombank.task.randomuser.api.Specifications;
import shepelev.denis.sovcombank.task.randomuser.utils.TestDataFactory;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Позитивные тесты
 **/
public class PositiveTests extends AbstractTest {
    /**
     * Проверить имя
     **/
    @Test
    public void getUserVerifyName() {
        Root response = getUsersFromSeed();
        assertEquals(TestDataFactory.getTestDataName(), response.results.get(0).name);
    }

    /**
     * Выставить спецификацию
     **/
    @BeforeClass
    public static void setSpecifications() {
        Specifications.installSpecification(Specifications.requestSpecification(),
                Specifications.responseSpecificationOk200());
    }
}
