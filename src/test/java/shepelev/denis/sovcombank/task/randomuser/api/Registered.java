package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Registered {
    public Date date;
    public int age;
}
