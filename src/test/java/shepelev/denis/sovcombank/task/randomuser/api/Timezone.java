package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Timezone {
    public String offset;
    public String description;
}
