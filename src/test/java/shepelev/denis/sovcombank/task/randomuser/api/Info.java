package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Info {
    public String seed;
    public Integer results;
    public Integer page;
    public String version;
}
