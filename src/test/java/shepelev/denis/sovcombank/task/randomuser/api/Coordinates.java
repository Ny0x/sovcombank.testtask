package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Coordinates {
    public String latitude;
    public String longitude;
}
