package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Dob {
    public Date date;
    public Integer age;
}
