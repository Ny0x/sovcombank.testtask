package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Name {
    public String title;
    public String first;
    public String last;
}
