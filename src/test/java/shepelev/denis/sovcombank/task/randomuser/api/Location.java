package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Location {
    public Street street;
    public String city;
    public String state;
    public String country;
    public String postcode;
    public Coordinates coordinates;
    public Timezone timezone;
}
