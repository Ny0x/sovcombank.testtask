package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Picture {
    public String large;
    public String medium;
    public String thumbnail;
}
