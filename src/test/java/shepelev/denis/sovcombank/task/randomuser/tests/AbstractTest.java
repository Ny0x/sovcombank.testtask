package shepelev.denis.sovcombank.task.randomuser.tests;

import shepelev.denis.sovcombank.task.randomuser.Config;
import shepelev.denis.sovcombank.task.randomuser.api.Root;

import static io.restassured.RestAssured.given;

/**
 * Абстрактный тест
 **/
public class AbstractTest {
    /**
     * Получить одного и того же юзера при каждом новом запуске
     **/
    protected Root getUsersFromSeed() {
        return given()
                .when()
                .get(Config.apiVersion + "/?seed=foobar")
                .then()
                .log().body()
                .extract().as(Root.class);
    }
}
