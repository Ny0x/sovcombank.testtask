package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

import java.util.ArrayList;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Root {
    public ArrayList<Result> results;
    public Info info;
}
