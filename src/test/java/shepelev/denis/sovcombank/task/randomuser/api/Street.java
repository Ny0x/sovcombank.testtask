package shepelev.denis.sovcombank.task.randomuser.api;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Street {
    public int number;
    public String name;
}
