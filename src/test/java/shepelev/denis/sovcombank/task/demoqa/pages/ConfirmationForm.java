package shepelev.denis.sovcombank.task.demoqa.pages;

import lombok.Getter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Форма подтверждения
 **/
@Getter
public class ConfirmationForm extends AbstractPage {
    @FindBy(xpath = "//*[text()='Student Name']/../td[2]")
    private WebElement nameAndSurname;
    @FindBy(xpath = "//*[text()='Student Email']/../td[2]")
    private WebElement email;
    @FindBy(xpath = "//*[text()='Gender']/../td[2]")
    private WebElement gender;
    @FindBy(xpath = "//*[text()='Mobile']/../td[2]")
    private WebElement mobile;
    @FindBy(xpath = "//*[text()='Date of Birth']/../td[2]")
    private WebElement dateOfBirth;
    @FindBy(xpath = "//*[text()='Subjects']/../td[2]")
    private WebElement subjects;
    @FindBy(xpath = "//*[text()='Hobbies']/../td[2]")
    private WebElement hobbies;
    @FindBy(xpath = "//*[text()='Address']/../td[2]")
    private WebElement address;
    @FindBy(xpath = "//*[text()='State and City']/../td[2]")
    private WebElement stateAndCity;


    public ConfirmationForm() {
        PageFactory.initElements(driver, this);
    }
}
