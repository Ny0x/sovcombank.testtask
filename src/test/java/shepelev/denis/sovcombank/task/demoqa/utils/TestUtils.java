package shepelev.denis.sovcombank.task.demoqa.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Вспомогательный класс для промежуточных вычислений (чтобы не забивать ими тесты)
 **/
public class TestUtils {
    public static String getFullField(String firstField, String secondField) {
        return firstField + " " + secondField;
    }

    public static <T> String getListAsString(List<T> collection) {
        return StringUtils.join(collection.stream().map(Object::toString).collect(Collectors.toList()), ", ");
    }
}
