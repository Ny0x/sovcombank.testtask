package shepelev.denis.sovcombank.task.demoqa.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Перечисление хобби
 **/
@AllArgsConstructor
@Getter
public enum HobbiesEnum {
    SPORTS("Sports"),
    READING("Reading"),
    MUSIC("Music");

    public final String label;

    @Override
    public String toString() {
        return label;
    }
}
