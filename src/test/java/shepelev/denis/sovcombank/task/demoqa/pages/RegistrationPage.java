package shepelev.denis.sovcombank.task.demoqa.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import shepelev.denis.sovcombank.task.demoqa.Config;
import shepelev.denis.sovcombank.task.demoqa.enums.GendersEnum;
import shepelev.denis.sovcombank.task.demoqa.enums.HobbiesEnum;

import java.util.List;

/**
 * Форма регистрации
 **/
public class RegistrationPage extends AbstractPage {
    // TODO: добавить аватар
    @FindBy(id = "firstName")
    private WebElement firstName;
    @FindBy(id = "lastName")
    private WebElement lastName;
    @FindBy(id = "userEmail")
    private WebElement email;
    @FindBy(id = "gender-radio-1")
    private WebElement genderMale;
    @FindBy(id = "gender-radio-2")
    private WebElement genderFemale;
    @FindBy(id = "gender-radio-3")
    private WebElement genderOther;
    @FindBy(id = "userNumber")
    private WebElement mobile;
    @FindBy(id = "dateOfBirthInput")
    private WebElement dateOfBirthInput;
    @FindBy(xpath = "//*[@id=\"dateOfBirth\"]//div[text()='20']")
    private WebElement dateOfBirthValue;
    @FindBy(id = "subjectsInput")
    private WebElement subjectsInput;
    @FindBy(id = "hobbies-checkbox-1")
    private WebElement sportsCheckbox;
    @FindBy(id = "hobbies-checkbox-2")
    private WebElement readingCheckbox;
    @FindBy(id = "hobbies-checkbox-3")
    private WebElement musicCheckbox;
    @FindBy(id = "currentAddress")
    private WebElement currentAddress;
    @FindBy(id = "react-select-3-input")
    private WebElement stateInput;
    @FindBy(id = "react-select-4-input")
    private WebElement cityInput;
    @FindBy(id = "submit")
    private WebElement submit;

    public RegistrationPage() {
        driver.get(Config.registrationPageUrl);
        PageFactory.initElements(driver, this);
    }

    public RegistrationPage fillFirstName(String firstName) {
        this.firstName.sendKeys(firstName);
        return this;
    }

    public RegistrationPage fillLastName(String lastName) {
        this.lastName.sendKeys(lastName);
        return this;
    }

    public RegistrationPage fillEmail(String email) {
        this.email.sendKeys(email);
        return this;
    }

    public RegistrationPage fillGender(GendersEnum gender) {
        switch (gender) {
            case MALE:
                new Actions(driver).click(genderMale).perform();
                break;
            case FEMALE:
                new Actions(driver).click(genderFemale).perform();
                break;
            default:
                new Actions(driver).click(genderOther).perform();
                break;
        }
        return this;
    }

    public RegistrationPage fillMobile(String mobile) {
        this.mobile.sendKeys(mobile);
        return this;
    }

    public RegistrationPage fillDateOfBirth() {
        this.dateOfBirthInput.click();
        new Actions(driver).click(dateOfBirthValue).perform();
        return this;
    }

    public RegistrationPage fillSubjects(List<String> subjects) {
        subjects.forEach(x -> {
            subjectsInput.sendKeys(x);
            subjectsInput.sendKeys(Keys.TAB);
        });
        return this;
    }

    public RegistrationPage fillHobbies(List<HobbiesEnum> hobbies) {
        for (HobbiesEnum hobie : hobbies) {
            switch (hobie) {
                case SPORTS:
                    new Actions(driver).click(sportsCheckbox).perform();
                    break;
                case READING:
                    new Actions(driver).click(readingCheckbox).perform();
                    break;
                case MUSIC:
                    new Actions(driver).click(musicCheckbox).perform();
                    break;
            }
        }
        return this;
    }

    public RegistrationPage fillCurrentAddress(String currentAddress) {
        this.currentAddress.sendKeys(currentAddress);
        return this;
    }

    public RegistrationPage fillState(String state) {
        this.stateInput.sendKeys(state);
        this.stateInput.sendKeys(Keys.TAB);
        return this;
    }

    public RegistrationPage fillCity(String city) {
        this.cityInput.sendKeys(city);
        this.cityInput.sendKeys(Keys.TAB);
        return this;
    }

    public ConfirmationForm submitForm() {
        new Actions(driver).click(submit).perform();
        return new ConfirmationForm();
    }
}
