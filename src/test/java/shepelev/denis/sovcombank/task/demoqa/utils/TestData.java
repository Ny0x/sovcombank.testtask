package shepelev.denis.sovcombank.task.demoqa.utils;

import shepelev.denis.sovcombank.task.demoqa.enums.GendersEnum;
import shepelev.denis.sovcombank.task.demoqa.enums.HobbiesEnum;

import java.util.List;

/**
 * Тестовые данные (знаю, по-хорошему нужно переделать, потому что расширяемости вообще нет)
 **/
public class TestData {
    public static String FIRST_NAME = "Ivan";
    public static String LAST_NAME = "Ivanov";
    public static String EMAIL = "test@gmail.com";
    public static GendersEnum GENDER = GendersEnum.MALE;
    public static String MOBILE = "1234567890";
    public static List<String> SUBJECTS = List.of("English", "Hindi");
    public static List<HobbiesEnum> HOBBIES = List.of(HobbiesEnum.SPORTS, HobbiesEnum.READING, HobbiesEnum.MUSIC);
    public static String CURRENT_ADDRESS = "Test Address";
    public static String STATE = "NCR";
    public static String CITY = "Delhi";
}
