package shepelev.denis.sovcombank.task.demoqa.tests;

import org.junit.Assert;
import org.junit.Test;
import shepelev.denis.sovcombank.task.demoqa.pages.ConfirmationForm;
import shepelev.denis.sovcombank.task.demoqa.pages.RegistrationPage;
import shepelev.denis.sovcombank.task.demoqa.utils.TestData;
import shepelev.denis.sovcombank.task.demoqa.utils.TestUtils;

/**
 * Позитивные тесты
 **/
public class RegistrationPagePositiveTests extends AbstractTest {
    @Test
    public void checkConfirmationFormAllFieldsFilledExceptAvatar() {
        ConfirmationForm confirmationForm = new RegistrationPage()
                .fillFirstName(TestData.FIRST_NAME)
                .fillLastName(TestData.LAST_NAME)
                .fillEmail(TestData.EMAIL)
                .fillGender(TestData.GENDER)
                .fillMobile(TestData.MOBILE)
                .fillDateOfBirth()
                .fillSubjects(TestData.SUBJECTS)
                .fillHobbies(TestData.HOBBIES)
                .fillCurrentAddress(TestData.CURRENT_ADDRESS)
                .fillState(TestData.STATE)
                .fillCity(TestData.CITY)
                .submitForm();

        Assert.assertEquals(TestUtils.getFullField(TestData.FIRST_NAME, TestData.LAST_NAME),
                confirmationForm.getNameAndSurname().getText());
        Assert.assertEquals(TestData.EMAIL, confirmationForm.getEmail().getText());
        Assert.assertEquals(TestData.GENDER.label, confirmationForm.getGender().getText());
        Assert.assertEquals(TestData.MOBILE, confirmationForm.getMobile().getText());
        Assert.assertEquals(TestUtils.getListAsString(TestData.SUBJECTS), confirmationForm.getSubjects().getText());
        Assert.assertEquals(TestUtils.getListAsString(TestData.HOBBIES), confirmationForm.getHobbies().getText());
        Assert.assertEquals(TestData.CURRENT_ADDRESS, confirmationForm.getAddress().getText());
        Assert.assertEquals(TestUtils.getFullField(TestData.STATE, TestData.CITY), confirmationForm.getStateAndCity().getText());
    }
}
