package shepelev.denis.sovcombank.task.demoqa.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Перечисление гендеров
 **/
@AllArgsConstructor
@Getter
public enum GendersEnum {
    MALE("Male"),
    FEMALE("Female"),
    OTHER("Other");

    public final String label;

    @Override
    public String toString() {
        return label;
    }
}
