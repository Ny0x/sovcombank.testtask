package shepelev.denis.sovcombank.task.demoqa;

/**
 * Класс с конфигурацией
 **/
public class Config {
    public static final String registrationPageUrl = "https://demoqa.com/automation-practice-form";
}
